﻿using Api.Data.Contexts;
using Api.Main.Authentication;
using Api.Main.Importers;
using Api.Main.Model;
using Api.Main.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Api.Main
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.Secrets.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            string connectionString = Configuration.GetConnectionString("DefaultConnection");
            services.Configure<ApplicationConfiguration>(Configuration.GetSection("Application"));

            services.AddDbContext<RegisteredResponseContext>(options => options.UseSqlServer(connectionString));
            services.AddScoped<MockService>();
            services.AddScoped<ImportStorer>();
            services.AddScoped<BasicAuthorize>();

            services.Configure<DefaultSettings>(defaultSettings => Configuration.GetSection("DefaultSettings").Bind(defaultSettings));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            // apply migratiosn if not applied (create if not exists too)
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                try
                {
                    Console.Write("Upgrading database...");
                    var context = serviceScope.ServiceProvider.GetRequiredService<RegisteredResponseContext>();
                    context.Database.Migrate();

                    Console.WriteLine("Complete.");
                
                    Console.Write("Seeding database...");
                    // seed data
                    JsonImporter importer = new JsonImporter();
                    var users = importer.GetUsers(@"Seed\users.json");
                    ImportStorer storer = new ImportStorer(context,
                        serviceScope.ServiceProvider.GetRequiredService<MockService>());
                    var defaultSettingsSection = Configuration.GetSection("DefaultSettings");
                    DefaultSettings defaultSettings = new DefaultSettings();
                    defaultSettingsSection.Bind(defaultSettings);

                    Console.WriteLine("Complete.");

                    storer.SeedUsersIfNotExist(users, defaultSettings.Branch, defaultSettings.TTL);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error trying to seed data:\n" + ex.Message);
                }
            }

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseCors(builder => builder.AllowAnyHeader().AllowAnyOrigin().AllowAnyMethod());
            app.UseMvc();
        }
    }
}

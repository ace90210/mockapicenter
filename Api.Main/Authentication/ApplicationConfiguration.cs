﻿namespace Api.Main.Authentication
{
    public class ApplicationConfiguration
    {
        public BasicConfiguration Basic { get; set; }


        public ApplicationConfiguration()
        {
            Basic = new BasicConfiguration();
        }

        public class BasicConfiguration
        {
            public string Username { get; set; }
            public string Password { get; set; }

            public BasicConfiguration()
            {

            }
        }
    }
}
﻿using System;
using Api.Data.Contexts;
using Api.Data.Models;
using Api.Data.Models.Import;
using Api.Main.Services;
using MockApiCenter.Controllers;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Api.Main.Importers
{
    public class ImportStorer
    {
        private const string baseAuthMock = @"{
		        ""sub"": ""<<sub>>"",
		        ""email"": ""<<email>>"",
		        ""given_name"": ""<<given_name>>"",
		        ""family_name"": ""<<family_name>>"",
		        ""phone_number"": ""<<phone_number>>"",
		        ]
	        }";

        private RegisteredResponseContext responseContext;
        private MockService mockService;

        public ImportStorer(RegisteredResponseContext responseContext, MockService mockService)
        {
            this.responseContext = responseContext;
            this.mockService = mockService;
        }

        public void SeedUsersIfNotExist(List<User> users, string tenantKey, TimeSpan ttl)
        {
            // check tenant doesnt already exist
            bool isNew = false;
            RegisteredTenant tenant = responseContext.RegisteredTenants.FirstOrDefault(rt => rt.Container.Equals(tenantKey));

            if (tenant == null)
            {
                isNew = true;
                tenant = new RegisteredTenant()
                {
                    Container = tenantKey,
                    MockCalls = new List<MockCall>()
                };
            }

            // Seed no response
            MockCall failedAuthMock = new MockCall()
            {
                RestType = RestType.GET,
                FromUrl = "userinfo",
                FromBody = null,
                ExactUrlMatch = true,
                ExpectAuthHeader = false,
                TTL = ttl,
                Response = new Response()
                {
                    Body = "",
                    Code = HttpStatusCode.Unauthorized
                }
            };

            //check filaed auth doesnt exist
            var existing = mockService.LookupResponse(tenantKey, failedAuthMock.RestType, failedAuthMock.FromUrl, failedAuthMock.FromBody, false);
            if (existing == Defaults.Response)
            {
                tenant.MockCalls.Add(failedAuthMock);
            }

            foreach (User user in users)
            {
                Response standardAuthResponse = CreateStandardResponse(user);
                Response standardUserInfoResponse = CreateStandardResponse(user);

                var authMock = CreateAuthMock(standardAuthResponse, user, ttl);
                var userInfoMock = CreateUserInfoMock(standardUserInfoResponse, user, ttl);

                //check exists Auth
                var existingAuth = mockService.LookupResponse(tenantKey, RestType.POST, authMock.FromUrl,
                    authMock.FromBody, false);
                if (existingAuth == Defaults.Response)
                {
                    tenant.MockCalls.Add(authMock);
                }

                //check exists UserInfo
                var existingUserInfo = mockService.LookupResponse(tenantKey, RestType.GET, userInfoMock.FromUrl,
                    userInfoMock.FromBody, false);
                if (existingUserInfo == Defaults.Response)
                {
                    tenant.MockCalls.Add(userInfoMock);
                }
            }

            if (isNew)
            {
                responseContext.RegisteredTenants.Add(tenant);
            }
            else
            {
                responseContext.RegisteredTenants.Update(tenant);
            }

            responseContext.SaveChanges();
        }

        private Response CreateStandardResponse(User user)
        {

            string body = baseAuthMock
                .Replace("<<sub>>", user.SubjectId)
                .Replace("<<email>>", user.GetClaimByType("email")?.Value)
                .Replace("<<given_name>>", user.GetClaimByType("given_name")?.Value)
                .Replace("<<family_name>>", user.GetClaimByType("family_name")?.Value)
                .Replace("<<phone_number>>", user.GetClaimByType("phone_number")?.Value);

            //gen response auth
            return new Response()
            {
                Body = body,
                ContentType = null,
                Code = HttpStatusCode.OK,
                Encoding = 0
            };
        }

        private MockCall CreateAuthMock(Response response, User user, TimeSpan ttl)
        {
            string fromBody = null;

            //gen auth username password
            if (string.IsNullOrWhiteSpace(user.Password))
            {
                fromBody = "{\"username\":\"" + user.Username + "\"}";
            }
            else
            {
                fromBody = "{\"username\":\"" + user.Username + "\",\"password\":\"" + user.Password + "\"}";
            }

            //gen mock auth
            return new MockCall()
            {
                FromUrl = "authenticate",
                RestType = RestType.POST,
                FromBody = fromBody,
                ExactUrlMatch = true,
                ExpectAuthHeader = false,
                Response = response,
                TTL = ttl
            };
        }

        private MockCall CreateUserInfoMock(Response response, User user, TimeSpan ttl)
        {
            //gen mock auth
            return new MockCall()
            {
                FromUrl = $"userinfo?issuer={user.ProviderName}&Subject={user.SubjectId}",
                RestType = RestType.GET,
                FromBody = null,
                ExactUrlMatch = true,
                ExpectAuthHeader = false,
                Response = response,
                TTL = ttl
            };
        }
    }
}

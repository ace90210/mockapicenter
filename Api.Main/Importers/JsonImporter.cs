﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Api.Data.Models.Import;
using Newtonsoft.Json;

namespace Api.Main.Importers
{
    public class JsonImporter : IUserImporter
    {

        public List<User> GetUsers(string from)
        {
            string data = null;
            if(File.Exists(from))
                data = File.ReadAllText(from);

            if (!string.IsNullOrWhiteSpace(data))
            {
                RegisteredUsers registeredUsers = JsonConvert.DeserializeObject<RegisteredUsers>(data);
                return registeredUsers?.Users?.ToList();
            }

            return null;
        }
    }
}

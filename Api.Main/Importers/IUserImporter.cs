﻿using System.Collections.Generic;
using Api.Data.Models.Import;

namespace Api.Main.Importers
{
    public interface IUserImporter
    {
        List<User> GetUsers(string from);
    }
}

﻿using Api.Data.Contexts;
using Api.Data.Models;
using Api.Main.Model;
using Api.Main.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Api.Main.Controllers
{
    [Route("api/[controller]")]
    public class AdminController : Controller
    {
        private DefaultSettings _defaultSettings;

        private RegisteredResponseContext ResponseContext { get; set; }
        
        public AdminController(RegisteredResponseContext responseContext, IOptions<DefaultSettings> defaultOptions, MockService mockService)
        {
            this._defaultSettings = defaultOptions?.Value ?? throw new ArgumentException("default settings not registered/found");
            ResponseContext = responseContext;
        }

        [HttpGet]
        public ContentResult GetTenants()
        {
            var tenants = ResponseContext.RegisteredTenants.ToList();

            var response = Content(JsonConvert.SerializeObject(tenants, Formatting.Indented), "application/json");
            response.StatusCode = 200;
            return response;
        }

        // GET api/values
        [HttpGet("{tenant}")]
        public IEnumerable<MockCall> Get(int tenant)
        {
            var tenantRecord = ResponseContext.RegisteredTenants.Include(t => t.MockCalls).FirstOrDefault(t => t.ID == tenant);

            if (tenantRecord == null)
            {
                return null;
            }

            return ResponseContext.MockCalls
                .Include(m => m.RegisteredTenant)
                .Include(i => i.Response)
                .Where(t => t.RegisteredTenant.ID == tenant);
        }

        // GET api/values/5
        [HttpGet("{tenant}/{id}")]
        public MockCall Get(int tenant, int id)
        {
            var tenantRecord = ResponseContext.RegisteredTenants.Include(t => t.MockCalls).FirstOrDefault(t => t.ID == tenant);

            if (tenantRecord == null)
            {
                return null;
            }

            return ResponseContext.MockCalls
                .Include(m => m.RegisteredTenant)
                .Include(s=> s.Response).FirstOrDefault(s =>TryValidateModel( s.RegisteredTenant.ID == tenant &&  s.ID == id));
        }

        // POST api/values
        [HttpPost("tenant/{*container}")]
        public int AddTenant(string container)
        {
            var existingContainer = ResponseContext.RegisteredTenants.Include(t => t.MockCalls).FirstOrDefault(t => t.Container.ToLower().Equals(container));

            if (existingContainer != null)
            {
                //exists so return this tenant
                return existingContainer.ID;
            }

            if (ModelState.IsValid && !string.IsNullOrWhiteSpace(container))
            {
                RegisteredTenant newTenant = new RegisteredTenant() {Container = container};
                ResponseContext.RegisteredTenants.Add(newTenant);
                ResponseContext.SaveChanges();
                return newTenant.ID;
            }

            return -1;
        }

        // POST api/values
        [HttpPost("{tenant}")]
        public int Post(int tenant, [FromBody]MockCall mockCall)
        {
            var tenantRecord = ResponseContext.RegisteredTenants.Include(t => t.MockCalls).FirstOrDefault(t => t.ID == tenant);

            if (tenantRecord == null)
            {
                return -1;
            }

            if (ModelState.IsValid)
            {
                if (mockCall.ID != 0)
                {
                    return mockCall.ID;
                }


                if (mockCall.TTL == null)
                {
                    mockCall.TTL = _defaultSettings.TTL;
                }

                tenantRecord.MockCalls.Add(mockCall);
                ResponseContext.RegisteredTenants.Update(tenantRecord);
                ResponseContext.SaveChanges();
            }

            return mockCall?.ID ?? -1;
        }

        // PUT api/values/5
        [HttpPut("{tenant}/{id}")]
        public int Put(int tenant, int id, [FromBody]MockCall value)
        {
            var tenantRecord = ResponseContext.RegisteredTenants.Include(t => t.MockCalls).FirstOrDefault(t => t.ID == tenant);

            if (tenantRecord == null)
            {
                return -1;
            }

            if (ModelState.IsValid)
            {
                if(id != value.ID)
                {
                    return -1;
                }

                var existing = tenantRecord.MockCalls.FirstOrDefault(s => s.ID == value.ID);

                if (value.TTL == null)
                {
                    value.TTL = _defaultSettings.TTL;
                }

                if (existing == null)
                {
                    tenantRecord.MockCalls.Add(value);
                    ResponseContext.RegisteredTenants.Update(tenantRecord);
                }
                else
                {
                    // ensure assigned to tenant
                    var entity = ResponseContext.MockCalls.Find(existing.ID);

                    ResponseContext.Responses.Add(value.Response);

                    ResponseContext.SaveChanges();
                    value.RegisteredTenantID = tenantRecord.ID;
                    value.ResponseId = value.Response.ID;
                    ResponseContext.Entry(entity).CurrentValues.SetValues(value);
                }
                ResponseContext.SaveChanges();
                return value.ID;
            }

            return -1;
        }

        // DELETE api/values/5
        [HttpDelete("{tenant}/{id}")]
        public void Delete(int tenant, int id)
        {
            var tenantRecord = ResponseContext.RegisteredTenants.Include(t => t.MockCalls).FirstOrDefault(t => t.ID == tenant);

            if (tenantRecord == null)
            {
                return;
            }

            var existing = ResponseContext.MockCalls.Include(mc => mc.RegisteredTenant).FirstOrDefault(s => s.ID == id);

            if (existing != null && existing.RegisteredTenant.ID == tenant)
            {
                ResponseContext.MockCalls.Remove(existing);
                ResponseContext.SaveChanges();
            }
        }
    }
}

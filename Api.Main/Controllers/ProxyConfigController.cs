﻿using Api.Data.Contexts;
using Api.Data.Models;
using Api.Main.Model;
using Api.Main.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Api.Main.Controllers
{
    [Route("api/[controller]")]
    public class ProxyConfigController : Controller
    {
        private DefaultSettings _defaultSettings;

        private RegisteredResponseContext ResponseContext { get; set; }
        
        public ProxyConfigController(RegisteredResponseContext responseContext, IOptions<DefaultSettings> defaultOptions, MockService mockService)
        {
            this._defaultSettings = defaultOptions?.Value ?? throw new ArgumentException("default settings not registered/found");
            ResponseContext = responseContext;
        }

        [HttpGet]
        public ContentResult GetProxies()
        {
            var proxies = ResponseContext.Proxies.ToList();

            var response = Content(JsonConvert.SerializeObject(proxies, Formatting.Indented), "application/json");
            response.StatusCode = 200;
            return response;
        }

        [HttpGet("{tenantid}")]
        public ContentResult GetProxiesForTenant(int tenantid)
        {
            var proxies = ResponseContext.Proxies.Where(p => p.RegisteredTenantID == tenantid).ToList();

            var response = Content(JsonConvert.SerializeObject(proxies, Formatting.Indented), "application/json");
            response.StatusCode = 200;
            return response;
        }

        // GET api/values
        [HttpGet("proxy/{proxyId}")]
        public Proxy Get(int proxyId)
        {
            var proxyRecord = ResponseContext.Proxies.Include(p => p.RegisteredTenant).FirstOrDefault(p => p.ID == proxyId);

            if (proxyRecord == null)
            {
                return null;
            }

            return proxyRecord;
        }

        public class ProxyModel
        {
            public string Url { get; set; }
        }

        // POST api/values
        [HttpPost("add/{tenantId}/{delay}")]
        public int AddProxy(int tenantId, int delay, [FromBody]ProxyModel proxyModel)
        {
            var tenant = ResponseContext.RegisteredTenants.Include(t => t.Proxies).FirstOrDefault(t => t.ID == tenantId);

            if (tenant != null) // and check proxy exists &&)
            {
                //exists so return this tenant
                ///return existingContainer.ID;
            }

            if (tenant != null && !string.IsNullOrWhiteSpace(proxyModel?.Url) && ModelState.IsValid)
            {
                Proxy newProxy = new Proxy() { FakeDelay = delay < 0 ? 0 : delay, ToUrl = proxyModel.Url, RegisteredTenant = tenant };
                ResponseContext.Proxies.Add(newProxy);
                ResponseContext.SaveChanges();
                return newProxy.ID;
            }

            return -1;
        }

        // DELETE api/values/5
        [HttpDelete("{tenant}/{proxyId}")]
        public void Delete(int tenant, int proxyId)
        {
            var tenantRecord = ResponseContext.RegisteredTenants.Include(t => t.Proxies).FirstOrDefault(t => t.ID == tenant);

            if (tenantRecord == null)
            {
                return;
            }

            var existing = ResponseContext.Proxies.Include(mc => mc.RegisteredTenant).FirstOrDefault(s => s.ID == proxyId);

            if (existing != null && existing.RegisteredTenant.ID == tenant)
            {
                ResponseContext.Proxies.Remove(existing);
                ResponseContext.SaveChanges();
            }
        }
    }
}

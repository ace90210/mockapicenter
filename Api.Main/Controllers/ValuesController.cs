﻿using System.Collections.Generic;
using System.Linq;
using Api.Data.Contexts;
using Api.Data.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Api.Main.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        public RegisteredResponseContext ResponseContext { get; set; }

        public ValuesController(RegisteredResponseContext responseContext)
        {
            ResponseContext = responseContext;
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<MockCall> Get()
        {
            return ResponseContext.MockCalls.Include(i => i.Response);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public MockCall Get(int id)
        {
            return ResponseContext.MockCalls.Include(s=> s.Response).FirstOrDefault(s => s.ID == id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]MockCall mockCall)
        {
            if (ModelState.IsValid)
            {
                ResponseContext.MockCalls.Add(mockCall);
                ResponseContext.SaveChanges();
            }
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]MockCall value)
        {
            if (ModelState.IsValid)
            {
                var existing = ResponseContext.MockCalls.FirstOrDefault(s => s.ID == id);

                if (existing != null)
                {
                    existing = value;
                }
                else
                {
                    ResponseContext.MockCalls.Add(value);
                }
                ResponseContext.SaveChanges();
            }
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var existing = ResponseContext.MockCalls.FirstOrDefault(s => s.ID == id);

            if (existing != null)
            {
                ResponseContext.MockCalls.Remove(existing);
            }

            ResponseContext.SaveChanges();
        }
    }
}

﻿using Api.Data.Contexts;
using Api.Data.Models;
using Api.Main.Helpers;
using Api.Main.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MockApiCenter.Controllers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace Api.Main.Controllers
{
    [Route("api/[controller]")]
    public class ProxyController : Controller
    {
        private readonly RegisteredResponseContext _responseContext;
        private readonly MockService _mockService;


        public ProxyController(RegisteredResponseContext responseContext, MockService mockService)
        {
            _responseContext = responseContext;
            _mockService = mockService;
        }


        private async Task<HttpResponseMessage> SendRequestAsync(RegisteredTenant tenant, int proxyId, string endpointPath)
        {
            Proxy proxy = tenant.Proxies.FirstOrDefault(p => p.ID == proxyId);
            if (proxy != null)
            {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(proxy.ToUrl);
 
                    try
                    {
                        foreach (var header in HttpContext.Request.Headers.Where(h => h.Key == "Authorization"))
                        {
                            client.DefaultRequestHeaders.Add(header.Key, (IEnumerable<string>)header.Value);
                        }
                  
                return await client.GetAsync(endpointPath);
            }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw new ArgumentException("Proxy Error");
                    }
                }
            }

            throw new ArgumentException("Invalid Proxy");
        }

        #region GET
        [HttpGet("{tenantid}/{proxyid}/{*endpointPath}")]
        public async Task<ContentResult> Get(int tenantid, int proxyid, string endpointPath)
        {
            RegisteredTenant tenant = _responseContext.RegisteredTenants.Include(t => t.Proxies).FirstOrDefault(t => t.ID == tenantid);

            var response = await SendRequestAsync(tenant, proxyid, endpointPath);

            await CreateMockResultAsync(tenant, endpointPath, response);

            var contentReturned = await response?.Content?.ReadAsStringAsync();
            return Content(contentReturned, "application/json");
        }

        private async Task CreateMockResultAsync(RegisteredTenant tenant, string endpointPath, HttpResponseMessage response)
        {
            Console.WriteLine("Checking if mock exists");
            var existingResponse = MockResponseHelpers.LookupResponse(_mockService, HttpContext, RestType.GET, endpointPath);

            if (existingResponse != null && existingResponse != Defaults.Response)
            {
                Console.WriteLine("Existing mock found ignoring for now (TODO CONFIGURABLE BEHVAIOUR HERE)");
                return;
            }
            else
            {
                Console.WriteLine("No Mock found creating mock");

                var contentReturned = await response?.Content?.ReadAsStringAsync();

                MockCall mockCall = new MockCall()
                {
                    RegisteredTenant = tenant,
                    ExactUrlMatch = true,
                    FromUrl = endpointPath,
                    RestType = RestType.GET,
                    Response = new Response()
                    {
                        Body = contentReturned,
                        Code = response.StatusCode,
                        ContentType = "application/json",
                        Encoding = SupportedEncodingType.UTF8
                    }
                };
                _responseContext.MockCalls.Add(mockCall);
                await _responseContext.SaveChangesAsync();
            }
        }
        #endregion

        #region POST
        [HttpPost]
        public ContentResult Post()
        {
            var response = LookupResponse(RestType.POST, "");
            return ConvertResponse(response);
        }

        [HttpPost("{*endpointPath}")]
        [Produces("application/json")]
        public ContentResult Post(string endpointPath)
        {
            var response = LookupResponse(RestType.POST, endpointPath);
            return ConvertResponse(response);
        }
        #endregion

        #region PUT
        [HttpPut]
        public ContentResult Put()
        {
            var response = LookupResponse(RestType.POST, "");
            return ConvertResponse(response);
        }

        [HttpPut("{*endpointPath}")]
        public ContentResult Put(string endpointPath)
        {
            var response = LookupResponse(RestType.POST, endpointPath);
            return ConvertResponse(response);
        }
        #endregion

        #region DELETE
        [HttpDelete]
        public ContentResult Delete()
        {
            var response = LookupResponse(RestType.DELETE, "");
            return ConvertResponse(response);
        }

        [HttpDelete("{*endpointPath}")]
        public ContentResult Delete(string endpointPath)
        {
            var response = LookupResponse(RestType.DELETE, endpointPath);
            return ConvertResponse(response);
        }
        #endregion

        #region Helper
        private ContentResult ConvertResponse(Response r)
        {
            var response = Content(r.Body, r.ContentType ?? "text/plain");
            response.StatusCode = (int)r.Code;

            return response;
        }

        private Response LookupResponse(RestType restType, string endpointPath)
        {
            bool authHeaderPresent = HttpContext?.Request?.Headers.Any(h => h.Key.ToLower().Equals("authorization")) ?? false;

            string body = restType != RestType.GET ? RequestBody() : null;
            string queryString = HttpContext?.Request?.QueryString.ToString().ToLower();
            if (!string.IsNullOrWhiteSpace(queryString))
            {
                endpointPath += queryString;
            }

            if (endpointPath != null)
            {
                return _mockService.LookupResponse(restType, endpointPath, body, authHeaderPresent);
            }
            Console.WriteLine($"No result found for\tEnd point: {endpointPath}\tBody: {body}");
            return Defaults.Response;
        }

        public string RequestBody()
        {
            try
            {
                using (var reader = new StreamReader(Request.Body))
                {
                    return reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
        #endregion
    }
}

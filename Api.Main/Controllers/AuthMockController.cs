﻿using Api.Data.Models;
using Api.Main.Services;
using Microsoft.AspNetCore.Mvc;
using MockApiCenter.Controllers;
using System;
using System.IO;
using System.Linq;
using Api.Main.Authentication;

namespace Api.Main.Controllers
{
    [Route("api/[controller]")]
    [ServiceFilter(typeof(BasicAuthorize))]
    public class AuthMockController : Controller
    {
        private readonly MockService _mockService;

        public AuthMockController(MockService mockService)
        {
            _mockService = mockService;
        }
        #region GET

        [HttpGet]
        public ContentResult Get()
        {
            var response = LookupResponse(RestType.GET, "");
            return ConvertResponse(response);
        }

        [HttpGet("{*endpointPath}")]
        public ContentResult Get(string endpointPath)
        {
            var response = LookupResponse(RestType.GET, endpointPath);
            return ConvertResponse(response);
        }
        #endregion

        #region POST
        [HttpPost]
        public ContentResult Post()
        {
            var response = LookupResponse( RestType.POST, "");
            return ConvertResponse(response);
        }

        [HttpPost("{*endpointPath}")]
        [Produces("application/json")]
        public ContentResult Post(string endpointPath)
        {
            var response = LookupResponse(RestType.POST, endpointPath);
            return ConvertResponse(response);
        }
        #endregion

        #region PUT
        [HttpPut]
        public ContentResult Put()
        {
            var response = LookupResponse( RestType.POST, "");
            return ConvertResponse(response);
        }

        [HttpPut("{*endpointPath}")]
        public ContentResult Put(string endpointPath)
        {
            var response = LookupResponse(RestType.POST, endpointPath);
            return ConvertResponse(response);
        }
        #endregion

        #region DELETE
        [HttpDelete]
        public ContentResult Delete()
        {
            var response = LookupResponse( RestType.DELETE, "");
            return ConvertResponse(response);
        }

        [HttpDelete("{*endpointPath}")]
        public ContentResult Delete( string endpointPath)
        {
            var response = LookupResponse( RestType.DELETE, endpointPath);
            return ConvertResponse(response);
        }
        #endregion

        #region Helper
        private ContentResult ConvertResponse(Response r)
        {
            var response = Content(r.Body, r.ContentType ?? "text/plain");
            response.StatusCode = (int)r.Code;

            return response;
        }

        private Response LookupResponse(RestType restType, string endpointPath)
        {
            bool authHeaderPresent = HttpContext?.Request?.Headers.Any(h => h.Key.ToLower().Equals("authorization")) ?? false;

            string body = restType != RestType.GET ? RequestBody() : null;
            string queryString = HttpContext?.Request?.QueryString.ToString().ToLower();
            if (!string.IsNullOrWhiteSpace(queryString))
            {
                endpointPath += queryString;
            }

            if (endpointPath != null)
            {
                return _mockService.LookupResponse(restType, endpointPath, body, authHeaderPresent);
            }
            Console.WriteLine($"No result found for\tEnd point: {endpointPath}\tBody: {body}");
            return Defaults.Response;
        }

        public string RequestBody()
        {
            try
            {
                using (var reader = new StreamReader(Request.Body))
                {
                    return reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
        #endregion
    }
}

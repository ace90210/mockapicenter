﻿using Api.Data.Models;
using Api.Main.Helpers;
using Api.Main.Services;
using Microsoft.AspNetCore.Mvc;
using MockApiCenter.Controllers;
namespace Api.Main.Controllers
{
    [Route("api/[controller]")]
    public class MockController : Controller
    {
        private readonly MockService _mockService;
       
        public MockController(MockService mockService)
        {
            _mockService = mockService;
        }
        #region GET

        [HttpGet]
        public ContentResult Get()
        {
            var response = MockResponseHelpers.LookupResponse(_mockService, HttpContext, RestType.GET, "");
            return ConvertResponse(response);
        }

        [HttpGet("{*endpointPath}")]
        public ContentResult Get(string endpointPath)
        {
            var response = MockResponseHelpers.LookupResponse(_mockService, HttpContext, RestType.GET, endpointPath);
            return ConvertResponse(response);
        }
        #endregion

        #region POST
        [HttpPost]
        public ContentResult Post()
        {
            var response = MockResponseHelpers.LookupResponse(_mockService, HttpContext, RestType.POST, "");
            return ConvertResponse(response);
        }

        [HttpPost("{*endpointPath}")]
        [Produces("application/json")]
        public ContentResult Post(string endpointPath)
        {
            var response = MockResponseHelpers.LookupResponse(_mockService, HttpContext, RestType.POST, endpointPath);
            return ConvertResponse(response);
        }
        #endregion

        #region PUT
        [HttpPut]
        public ContentResult Put()
        {
            var response = MockResponseHelpers.LookupResponse(_mockService, HttpContext, RestType.POST, "");
            return ConvertResponse(response);
        }

        [HttpPut("{*endpointPath}")]
        public ContentResult Put(string endpointPath)
        {
            var response = MockResponseHelpers.LookupResponse(_mockService, HttpContext, RestType.POST, endpointPath);
            return ConvertResponse(response);
        }
        #endregion

        #region DELETE
        [HttpDelete]
        public ContentResult Delete()
        {
            var response = MockResponseHelpers.LookupResponse(_mockService, HttpContext, RestType.DELETE, "");
            return ConvertResponse(response);
        }

        [HttpDelete("{*endpointPath}")]
        public ContentResult Delete(string endpointPath)
        {
            var response = MockResponseHelpers.LookupResponse(_mockService, HttpContext, RestType.DELETE, endpointPath);
            return ConvertResponse(response);
        }
        #endregion

        #region Helper
        private ContentResult ConvertResponse(Response r)
        {
            var response = Content(r.Body, r.ContentType ?? "text/plain");
            response.StatusCode = (int)r.Code;

            return response;
        }
        #endregion
    }
}

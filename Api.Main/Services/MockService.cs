﻿using Api.Data.Contexts;
using Api.Data.Models;
using Microsoft.EntityFrameworkCore;
using MockApiCenter.Controllers;
using System;
using System.Linq;

namespace Api.Main.Services
{
    public class MockService
    {
        private readonly RegisteredResponseContext _responseContext;

        public MockService(RegisteredResponseContext responseContext)
        {
            _responseContext = responseContext;
        }


        public RegisteredTenant FindTenantByEndpoint(string endpoint)
        {
            return _responseContext.RegisteredTenants.Include(t => t.Proxies).Include(t => t.MockCalls).ThenInclude(i => i.Response)
                .FirstOrDefault(rt => endpoint.StartsWith(rt.Container));
        }

        public Response LookupResponse(RestType restType, string endpoint, string body, bool authHeaderPresent)
        {
            RegisteredTenant tenantRecord = FindTenantByEndpoint(endpoint);

            if (tenantRecord == null || tenantRecord.Container == null)
            {
                Console.WriteLine($"Warning: no tenant found for this path");
                return Defaults.Response;
            }

            //strip out container from endpoint
            endpoint = endpoint.Replace(tenantRecord.Container, "");

            endpoint = endpoint.TrimStart('/');

            return LookupResponse(tenantRecord.Container, restType, endpoint, body, authHeaderPresent);
        }

        public Response LookupResponse(string container, RestType restType, string endpoint, string body,
            bool authHeaderPresent)
        {
            CleanUpTllRecords();

            Console.WriteLine($"looking for container {container}");
            var tenantRecord = _responseContext.RegisteredTenants.Include(t => t.MockCalls).ThenInclude(i => i.Response)
                .FirstOrDefault(t => t.Container.Equals(container));

            if (tenantRecord == null)
            {
                Console.WriteLine($"Warning: container not found {container}");
                return Defaults.Response;
            }

            Console.WriteLine($"Mock call made on path {endpoint}, Body {body}");

            foreach (var mockCall in tenantRecord.MockCalls.Where(mc => mc.RestType == restType))
            {
                var response = CheckResponse(endpoint, body, authHeaderPresent, mockCall);
                if (response != null)
                {
                    Console.WriteLine($"Mock response found response id: {mockCall.ID}");
                    return response;
                }
            }

            Console.WriteLine($"No result found for\tEnd point: {endpoint}\tBody: {body}");
            return Defaults.Response;
        }

        public void CleanUpTllRecords()
        {
            var outdatedCalls = _responseContext.MockCalls.Include(mc => mc.Response)
                .Where(mc => IsOutOfDate(mc.TTL, mc.CreatedUtc));

            if (outdatedCalls.Any())
            {
                _responseContext.MockCalls.RemoveRange(outdatedCalls);
                _responseContext.SaveChanges();
            }
        }

        private bool IsOutOfDate(TimeSpan? ttl, DateTime created)
        {
            if (ttl == null)
                return false;

            TimeSpan timeSinceCreate = DateTime.UtcNow.Subtract(created);

            return timeSinceCreate > ttl;
        }

        private static Response CheckResponse(string endpoint, string body, bool authHeaderPresent, MockCall mockCall)
        {
            if (!mockCall.ExpectAuthHeader || (mockCall.ExpectAuthHeader && authHeaderPresent))
            {
                bool bodyMatches = body == null || body.ToLower().Equals(mockCall.FromBody?.ToLower());

                // Exact match check
                if (mockCall.ExactUrlMatch &&
                    mockCall.FromUrl.ToLower().Equals(endpoint.ToLower()) &&
                    bodyMatches)
                {
                    return mockCall.Response;
                }

                // starts with check
                if (!mockCall.ExactUrlMatch && endpoint.ToLower().StartsWith(mockCall.FromUrl.ToLower()) && bodyMatches)
                {
                    return mockCall.Response;
                }
            }
            return null;
        }
    }
}
﻿using Api.Data.Models;
using Api.Main.Services;
using Microsoft.AspNetCore.Http;
using MockApiCenter.Controllers;
using System;
using System.IO;
using System.Linq;

namespace Api.Main.Helpers
{
    public static class MockResponseHelpers
    {
        public static Response LookupResponse(MockService _mockService, HttpContext context, RestType restType, string endpointPath)
        {
            bool authHeaderPresent = context?.Request?.Headers.Any(h => h.Key.ToLower().Equals("authorization")) ?? false;

            string body = restType != RestType.GET ? RequestBody(context?.Request) : null;
            string queryString = context?.Request?.QueryString.ToString().ToLower();
            if (!string.IsNullOrWhiteSpace(queryString))
            {
                endpointPath += queryString;
            }

            if (endpointPath != null)
            {
                return _mockService.LookupResponse(restType, endpointPath, body, authHeaderPresent);
            }
            Console.WriteLine($"No result found for\tEnd point: {endpointPath}\tBody: {body}");
            return Defaults.Response;
        }
        

        public static string RequestBody(HttpRequest request)
        {
            try
            {
                using (var reader = new StreamReader(request.Body))
                {
                    return reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Main.Model
{
    public class DefaultSettings
    {
        public string Branch { get; set; }
        public TimeSpan TTL { get; set; }
    }
}

FROM microsoft/aspnetcore:2.0 AS base
WORKDIR /app
EXPOSE 5554
ENV ASPNETCORE_URLS=http://+:5554

FROM microsoft/aspnetcore-build:2.0 AS build
WORKDIR /src
COPY Api.Main/Api.Main.csproj Api.Main/

RUN dotnet restore Api.Main/Api.Main.csproj

COPY . ./

RUN dotnet build Api.Main/Api.Main.csproj -c Release -o /app

FROM build AS publish
RUN dotnet publish Api.Main/Api.Main.csproj -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "Api.Main.dll"]
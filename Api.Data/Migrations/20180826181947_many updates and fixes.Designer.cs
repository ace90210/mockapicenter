﻿// <auto-generated />
using Api.Data.Contexts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using MockApiCenter.Controllers;
using System;
using System.Net;

namespace Api.Data.Migrations
{
    [DbContext(typeof(RegisteredResponseContext))]
    [Migration("20180826181947_many updates and fixes")]
    partial class manyupdatesandfixes
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.3-rtm-10026")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Api.Data.Models.MockCall", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("ExactUrlMatch");

                    b.Property<bool>("ExpectAuthHeader");

                    b.Property<string>("FromBody");

                    b.Property<string>("FromUrl")
                        .IsRequired();

                    b.Property<int>("ResponseId");

                    b.Property<int>("RestType");

                    b.HasKey("ID");

                    b.HasIndex("ResponseId");

                    b.ToTable("MockCalls");
                });

            modelBuilder.Entity("MockApiCenter.Controllers.Response", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Body");

                    b.Property<int>("Code");

                    b.Property<string>("ContentType");

                    b.Property<int>("Encoding");

                    b.HasKey("ID");

                    b.ToTable("Responses");
                });

            modelBuilder.Entity("Api.Data.Models.MockCall", b =>
                {
                    b.HasOne("MockApiCenter.Controllers.Response", "Response")
                        .WithMany()
                        .HasForeignKey("ResponseId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}

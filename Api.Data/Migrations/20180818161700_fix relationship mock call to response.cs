﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Api.Data.Migrations
{
    public partial class fixrelationshipmockcalltoresponse : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MockCalls_Responses_ResponseID",
                table: "MockCalls");

            migrationBuilder.DropIndex(
                name: "IX_MockCalls_ResponseID",
                table: "MockCalls");

            migrationBuilder.DropColumn(
                name: "ResponseID",
                table: "MockCalls");

            migrationBuilder.AddColumn<int>(
                name: "MockCallId",
                table: "Responses",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Responses_MockCallId",
                table: "Responses",
                column: "MockCallId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Responses_MockCalls_MockCallId",
                table: "Responses",
                column: "MockCallId",
                principalTable: "MockCalls",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Responses_MockCalls_MockCallId",
                table: "Responses");

            migrationBuilder.DropIndex(
                name: "IX_Responses_MockCallId",
                table: "Responses");

            migrationBuilder.DropColumn(
                name: "MockCallId",
                table: "Responses");

            migrationBuilder.AddColumn<int>(
                name: "ResponseID",
                table: "MockCalls",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_MockCalls_ResponseID",
                table: "MockCalls",
                column: "ResponseID");

            migrationBuilder.AddForeignKey(
                name: "FK_MockCalls_Responses_ResponseID",
                table: "MockCalls",
                column: "ResponseID",
                principalTable: "Responses",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿// <auto-generated />
using Api.Data.Contexts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Api.Data.Migrations
{
    [DbContext(typeof(RegisteredResponseContext))]
    [Migration("20180815183437_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.3-rtm-10026")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("MockApiCenter.Controllers.MockCall", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("FromUrl");

                    b.Property<int?>("ResponseID");

                    b.Property<int>("RestType");

                    b.HasKey("ID");

                    b.HasIndex("ResponseID");

                    b.ToTable("MockCalls");
                });

            modelBuilder.Entity("MockApiCenter.Controllers.Response", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Body");

                    b.Property<int>("Code");

                    b.HasKey("ID");

                    b.ToTable("Responses");
                });

            modelBuilder.Entity("MockApiCenter.Controllers.MockCall", b =>
                {
                    b.HasOne("MockApiCenter.Controllers.Response", "Response")
                        .WithMany()
                        .HasForeignKey("ResponseID");
                });
#pragma warning restore 612, 618
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Api.Data.Migrations
{
    public partial class Addbodytomockcallrequirements : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "FromUrl",
                table: "MockCalls",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FromBody",
                table: "MockCalls",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FromBody",
                table: "MockCalls");

            migrationBuilder.AlterColumn<string>(
                name: "FromUrl",
                table: "MockCalls",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Api.Data.Migrations
{
    public partial class addbaseproxyconfig : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_MockCalls_ResponseId",
                table: "MockCalls");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedUtc",
                table: "MockCalls",
                nullable: false,
                oldClrType: typeof(DateTime),
                defaultValueSql: "GETUTCDATE()");

            migrationBuilder.CreateTable(
                name: "Proxies",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FakeDelay = table.Column<int>(nullable: false),
                    RegisteredTenantID = table.Column<int>(nullable: false),
                    ToUrl = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Proxies", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Proxies_RegisteredTenants_RegisteredTenantID",
                        column: x => x.RegisteredTenantID,
                        principalTable: "RegisteredTenants",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MockCalls_ResponseId",
                table: "MockCalls",
                column: "ResponseId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Proxies_RegisteredTenantID",
                table: "Proxies",
                column: "RegisteredTenantID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Proxies");

            migrationBuilder.DropIndex(
                name: "IX_MockCalls_ResponseId",
                table: "MockCalls");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedUtc",
                table: "MockCalls",
                nullable: false,
                defaultValueSql: "GETUTCDATE()",
                oldClrType: typeof(DateTime));

            migrationBuilder.CreateIndex(
                name: "IX_MockCalls_ResponseId",
                table: "MockCalls",
                column: "ResponseId");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Api.Data.Migrations
{
    public partial class manyupdatesandfixes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Responses_MockCalls_MockCallId",
                table: "Responses");

            migrationBuilder.DropIndex(
                name: "IX_Responses_MockCallId",
                table: "Responses");

            migrationBuilder.DropColumn(
                name: "MockCallId",
                table: "Responses");

            migrationBuilder.AddColumn<string>(
                name: "ContentType",
                table: "Responses",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Encoding",
                table: "Responses",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "ExpectAuthHeader",
                table: "MockCalls",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "ResponseId",
                table: "MockCalls",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_MockCalls_ResponseId",
                table: "MockCalls",
                column: "ResponseId");

            migrationBuilder.AddForeignKey(
                name: "FK_MockCalls_Responses_ResponseId",
                table: "MockCalls",
                column: "ResponseId",
                principalTable: "Responses",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MockCalls_Responses_ResponseId",
                table: "MockCalls");

            migrationBuilder.DropIndex(
                name: "IX_MockCalls_ResponseId",
                table: "MockCalls");

            migrationBuilder.DropColumn(
                name: "ContentType",
                table: "Responses");

            migrationBuilder.DropColumn(
                name: "Encoding",
                table: "Responses");

            migrationBuilder.DropColumn(
                name: "ExpectAuthHeader",
                table: "MockCalls");

            migrationBuilder.DropColumn(
                name: "ResponseId",
                table: "MockCalls");

            migrationBuilder.AddColumn<int>(
                name: "MockCallId",
                table: "Responses",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Responses_MockCallId",
                table: "Responses",
                column: "MockCallId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Responses_MockCalls_MockCallId",
                table: "Responses",
                column: "MockCallId",
                principalTable: "MockCalls",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

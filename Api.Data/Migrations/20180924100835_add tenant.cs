﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Api.Data.Migrations
{
    public partial class addtenant : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RegisteredTenantID",
                table: "MockCalls",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "RegisteredTenants",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Container = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RegisteredTenants", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MockCalls_RegisteredTenantID",
                table: "MockCalls",
                column: "RegisteredTenantID");

            migrationBuilder.CreateIndex(
                name: "IX_RegisteredTenants_Container",
                table: "RegisteredTenants",
                column: "Container",
                unique: true,
                filter: "[Container] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_MockCalls_RegisteredTenants_RegisteredTenantID",
                table: "MockCalls",
                column: "RegisteredTenantID",
                principalTable: "RegisteredTenants",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MockCalls_RegisteredTenants_RegisteredTenantID",
                table: "MockCalls");

            migrationBuilder.DropTable(
                name: "RegisteredTenants");

            migrationBuilder.DropIndex(
                name: "IX_MockCalls_RegisteredTenantID",
                table: "MockCalls");

            migrationBuilder.DropColumn(
                name: "RegisteredTenantID",
                table: "MockCalls");
        }
    }
}

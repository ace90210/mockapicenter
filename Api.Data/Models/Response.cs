﻿using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Text;
using Newtonsoft.Json;

namespace Api.Data.Models
{
    public enum SupportedEncodingType
    {
        UTF8,
        UNICODE,
        ACSCII
    }

    public static class ConvertHelper
    {
        public static Encoding Convert(SupportedEncodingType encodingType)
        {
            switch (encodingType)
            {
                case SupportedEncodingType.ACSCII: return Encoding.ASCII; 
                case SupportedEncodingType.UNICODE: return Encoding.Unicode; 
                case SupportedEncodingType.UTF8: return Encoding.UTF8; 
            }
            return Encoding.UTF8;
        }
    }

    public class Response
    {
        [Key]
        public int ID { get; set; }

        public HttpStatusCode Code { get; set; }

        public SupportedEncodingType Encoding { get; set; } = SupportedEncodingType.UTF8;

        public string ContentType { get; set; } = "text/plain";

        public string Body { get; set; }

        [JsonIgnore]
        public MockCall MockCall { get; set; }
    }
}
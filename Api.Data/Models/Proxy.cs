﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Api.Data.Models
{
    public class Proxy
    {
        [Key]
        public int ID { get; set; }

        public int RegisteredTenantID { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string ToUrl { get; set; } = string.Empty;

        public int FakeDelay { get; set; }

        [JsonIgnore]
        public RegisteredTenant RegisteredTenant { get; set; }
    }
}

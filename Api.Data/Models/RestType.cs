﻿namespace MockApiCenter.Controllers
{
    public enum RestType
    {
        GET,
        POST,
        PUT,
        DELETE
    }
}
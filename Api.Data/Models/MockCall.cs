﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MockApiCenter.Controllers;
using Newtonsoft.Json;

namespace Api.Data.Models
{
    public static class Defaults
    {
        private static Response response;

        public static Response Response {
            get
            {
                if (response == null)
                {
                    response = new Response()
                    {
                        Body = "{\n\t\"Warning\": \"[No mock setup configured for this end point]\"\n}",
                        Code = System.Net.HttpStatusCode.BadRequest
                    };
                }

                return response;
            }
        }
    }

    public class MockCall
    {
        [Key]
        public int ID { get; set; }

        public int RegisteredTenantID { get; set; }

        [Required(AllowEmptyStrings = true)]
        public string FromUrl { get; set; } = string.Empty;

        public bool ExactUrlMatch { get; set; }

        public bool ExpectAuthHeader { get; set; }

        public RestType RestType { get; set; }

        public string FromBody { get; set; }


        [Obsolete("Property 'Duration' should be used instead.")]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public long? TTLTicks { get; set; }

        // EF doesnt handle timespans greater than 24 hours correctly
        // Workaround to store ticks and convert
        [NotMapped]
        public TimeSpan? TTL
        {
#pragma warning disable 618
            get
            {
                if (TTLTicks == null)
                    return null;
                return new TimeSpan((long)TTLTicks);
            }
            set { TTLTicks = value?.Ticks; }
#pragma warning restore 618
        }
        private DateTime _createdUtc = DateTime.Now;
        [Required, DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedUtc {
            get {
                if(_createdUtc != null)
                    return _createdUtc;
                return DateTime.Now;
            }
            set
            {
                if (value != null)
                {
                    _createdUtc = value;
                }
            }
        }

        public int ResponseId { get; set; }

        public Response Response { get; set; }

        [JsonIgnore]
        public RegisteredTenant RegisteredTenant { get; set; }
    }
}
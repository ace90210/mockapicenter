﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Api.Data.Models
{
    public class RegisteredTenant
    {
        [Key]
        public int ID { get; set; }

        public string Container { get; set; }

        [JsonIgnore]
        public List<MockCall> MockCalls { get; set; }

        [JsonIgnore]
        public List<Proxy> Proxies { get; set; }
    }
}

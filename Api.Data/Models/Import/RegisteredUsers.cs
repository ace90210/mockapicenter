﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Data.Models.Import
{
    public class RegisteredUsers
    {
        public List<User> Users { get; set; }
    }
}

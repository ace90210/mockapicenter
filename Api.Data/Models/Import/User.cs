﻿using System.Collections.Generic;
using System.Linq;

namespace Api.Data.Models.Import
{
    public class User
    {
        public string ProviderName { get; set; }
        public string SubjectId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public List<Claim> Claims { get; set; }


        public Claim GetClaimByType(string type)
        {
            foreach (var claim in Claims)
            {
                if (claim?.Type?.Equals(type) ?? false)
                {
                    return claim;
                }
            }
            return null;
        }
    }
}
﻿namespace Api.Data.Models.Import
{
    public class Claim
    {
        public string Type { get; set; }
        public string Value { get; set; }
    }
}
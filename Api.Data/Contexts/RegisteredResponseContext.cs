﻿using Api.Data.Models;
using Microsoft.EntityFrameworkCore;
using MockApiCenter.Controllers;

namespace Api.Data.Contexts
{
    public class RegisteredResponseContext : DbContext
    {
        public RegisteredResponseContext(DbContextOptions<RegisteredResponseContext> options)
            : base(options)
        { }

        public DbSet<Response> Responses { get; set; }
        
        public DbSet<MockCall> MockCalls { get; set; }

        public DbSet<RegisteredTenant> RegisteredTenants { get; set; }

        public DbSet<Proxy> Proxies { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RegisteredTenant>()
                .HasIndex(rt => rt.Container)
                .IsUnique(true);
        }
    }
}
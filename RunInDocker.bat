@echo off

echo building image
start  /W docker build  -t edge_mocksms .

echo running container ctrl + c to exit
start  /W docker run --rm -t edge_mocksms

echo getting container name
docker ps -a -q --filter ancestor=edge_mocksms  > temp.txt
set /p containerName=<temp.txt
del temp.txt

echo container name: %containerName%

pause
echo all containers running
docker ps -a

echo stopping container %containerName%
start  /W docker stop %containerName%

echo list containers
docker container ls

pause